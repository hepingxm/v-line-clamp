function addStyle (el, binding) {
  el.style['-webkit-line-clamp'] = binding.value || 1
  el.style['word-break'] = 'break-all'
  el.style['text-overflow'] = 'ellipsis'
  el.style['overflow'] = 'hidden'
  el.style['display'] = '-webkit-box'
  el.style['-webkit-box-orient'] = 'vertical'
}

var lineClamp = {
  bind: addStyle,
  update: addStyle
}
export default {
  install: function (Vue) {
    Vue.directive('lineClamp', lineClamp)
  }
}

export {
  lineClamp
}
