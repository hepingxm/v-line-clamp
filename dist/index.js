(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.lineClamp = {}));
}(this, (function (exports) { 'use strict';

  function addStyle (el, binding) {
    el.style['-webkit-line-clamp'] = binding.value || 1;
    el.style['word-break'] = 'break-all';
    el.style['text-overflow'] = 'ellipsis';
    el.style['overflow'] = 'hidden';
    el.style['display'] = '-webkit-box';
    el.style['-webkit-box-orient'] = 'vertical';
  }

  var lineClamp = {
    bind: addStyle,
    update: addStyle
  };
  var index = {
    install: function (Vue) {
      Vue.directive('lineClamp', lineClamp);
    }
  };

  exports.default = index;
  exports.lineClamp = lineClamp;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
