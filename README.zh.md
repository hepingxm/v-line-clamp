### 当单词超过指定的行数时，使用省略号代替超出部分
## 用法

1.安装到你的项目中
~~~~
npm i v-line-clamp
// or
yarn add v-line-clamp
~~~~

2.在main.js入口中添加如下代码
~~~~
import LineClamp from 'v-line-clamp';

Vue.use(LineClamp)
~~~~

3.现在你就可以在vue模板中使用了
~~~~
<!-- By default, an ellipsis appears on one line -->
<div v-line-clamp></div>
<!-- And you can set a few lines to show the ellipsis -->
<div v-line-clamp="3"></div>
~~~~

### 也许你想直接在组件上使用，那么你可以按照下面的方式使用
~~~~
import { lineClamp } from 'v-line-clamp'

.....
    directives: {
        lineClamp
    }
....
~~~~

## 容易出现的问题点

也许你应该将当前指令的容器（div or 其他）设置style 为 display: inline-block or display: block
