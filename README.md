### When a word exceeds the specified number of lines, an ellipsis appears
[中文文档](https://gitee.com/hepingxm/v-line-clamp/blob/master/README.zh.md)
## usage

1.add to you project
~~~~
npm i v-line-clamp
// or
yarn add v-line-clamp
~~~~

2.in the main.js
~~~~
import LineClamp from 'v-line-clamp';

Vue.use(LineClamp)
~~~~

3.now you can use it in the template
~~~~
<!-- By default, an ellipsis appears on one line -->
<div v-line-clamp></div>
<!-- And you can set a few lines to show the ellipsis -->
<div v-line-clamp="3"></div>
~~~~

### Maybe you want to use it directly on components,and You can do this
~~~~
import { lineClamp } from 'v-line-clamp'

.....
    directives: {
        lineClamp
    }
....
~~~~

## Problems prone

maybe you should set it up display: inline-block or display: block
